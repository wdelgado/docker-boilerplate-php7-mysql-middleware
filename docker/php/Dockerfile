ARG PHP_VERSION=7.2-fpm-stretch
ARG COMPOSER_VERSION=latest

# builder stage
FROM php:$PHP_VERSION AS builder

ARG AMQP_VERSION=1.9.3
ARG PHPREDIS_VERSION=4.1.1

RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
        librabbitmq-dev \
        libxml2-dev \
        libzip-dev \
        procps \
        unzip \
        zip \
        iputils-ping \
    && docker-php-ext-install -j$(nproc) \
        soap \
        pdo_mysql \
        zip \
    && pecl install \
        amqp-$AMQP_VERSION \
        redis-$PHPREDIS_VERSION \
    && docker-php-ext-enable \
        amqp \
        redis \
    && rm -rf /tmp/* /var/lib/apt/lists/*

COPY ./docker/php/config/base.ini /usr/local/etc/php/conf.d/10-base.ini

WORKDIR /appdata

# development stage
FROM composer:$COMPOSER_VERSION AS composer
FROM builder AS php-composer-xdebug

ARG XDEBUG_VERSION=2.6.1

RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
        git \
        vim \
    && pecl install \
        xdebug-$XDEBUG_VERSION \
    && docker-php-ext-enable \
        xdebug \
    && rm -rf /tmp/* /var/lib/apt/lists/*

COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY ./docker/php/config/dev.ini /usr/local/etc/php/conf.d/50-dev.ini

RUN composer global require hirak/prestissimo

