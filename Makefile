include docker/docker.env
export $(shell sed 's/=.*//' docker/docker.env)

SHELL := /bin/bash

php_image_name := php-7.2-composer
php_container_name := php-container
elasticsearch_container_name := elasticsearch-container
kibana_container_name := kibana-container
mysql_container_name := mysql-container
rabbitmq_container_name := rabbitmq-container
redis_container_name := redis-container

status: 
	@docker ps -s

run:
	make build-image-php
	@docker-compose -f docker/main.yml up -d 

destroy: destroy-elasticstack destroy-rabbitmq destroy-redis destroy-mysql destroy-php 

destroy-php: .destroy-$(php_container_name)

destroy-elasticstack: .destroy-$(elasticsearch_container_name) .destroy-$(kibana_container_name)

destroy-rabbitmq: .destroy-$(rabbitmq_container_name)

destroy-redis: .destroy-$(redis_container_name)

destroy-mysql: .destroy-$(mysql_container_name)

restart: destroy run 

bash-php: 
	@docker exec -it $(php_container_name) bash

redis-cli: ## Open an interactive redis console in Redis container
	@docker exec -it $(redis_container_name) sh -c "redis-cli"

build-image-php:
	@docker build --tag $(php_image_name) --file docker/php/Dockerfile .	

create-network:
	@docker network create app-net 2> /dev/null || true	

.destroy-%:
	@docker kill $* 2> /dev/null || true
	@docker rm $* 2> /dev/null || true

