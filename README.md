Docker boilerplate: PHP7 + MySQL + Redis + RabbitMQ + Elastic Search + Kibana
=============================================================================

### Features
  - PHP 7.2 & composer
  - MySQL 5.7
  - Redis 4.0
  - RabbitMQ 3.6
  - Elastic Search 6.3.2
  - Kibana 6.3.2

### Usage
    Usage ./make [ run | bash-php | redis-cli | restart | destroy ] 
